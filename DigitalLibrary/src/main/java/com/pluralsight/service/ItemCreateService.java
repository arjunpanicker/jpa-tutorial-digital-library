package com.pluralsight.service;

import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;

import com.pluralsight.model.Author;
import com.pluralsight.model.Book;
import com.pluralsight.model.CD;
import com.pluralsight.model.Musician;
import com.pluralsight.utils.MyLogger;

public class ItemCreateService {
	
//	The @PersistenceContext won't work as it requires a JTA Data Source
//	@PersistenceContext(unitName = "digitalLibrary-persistenceUnit")
	private EntityManager entityManager;
	
	public ItemCreateService(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public CD createCD(CD cd) {
		entityManager.persist(cd);
		if(cd.getMusicians() != null) {
			for (Musician musician : cd.getMusicians()) {
				entityManager.persist(musician);
			}
		}
		
		MyLogger.log(Level.INFO, "Persisting CD successfull");
		return cd;
	}
	
//	The Book object in the arguments of the createBook() method is detached in nature 
//	and becomes managed when calling persist() method on it.
	public Book createBook(Book book) {
//		The Book entity becomes managed on calling the persist() method on it.
		entityManager.persist(book);
		if (book.getAuthors() != null) {
			for (Author author : book.getAuthors()) {
				entityManager.persist(author);
			}
		}
		
		MyLogger.log(Level.INFO, "Persisting Book successfull!");
		
		return book;
	}
	
	public void createBooks(Set<Book> books) {
		for (Book book : books) {
			entityManager.persist(book);
		}
		MyLogger.log(Level.INFO, "All Authors Persisted");
	}
	
}
