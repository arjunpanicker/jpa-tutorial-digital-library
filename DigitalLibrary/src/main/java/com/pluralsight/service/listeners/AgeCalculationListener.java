package com.pluralsight.service.listeners;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import com.pluralsight.model.Artist;

public class AgeCalculationListener {

	@PostLoad
	@PostPersist
	@PostUpdate
	public void calculateAge(Artist artist) {
		if(artist.getDateOfBirth() == null) {
			artist.setAge(null);
			return;
		}
		
		Calendar birth = new GregorianCalendar();
		birth.setTime(artist.getDateOfBirth());
		Calendar now = new GregorianCalendar();
		now.setTime(new Date());
		
		int adjust = 0;
		if(now.get(Calendar.DAY_OF_YEAR) - birth.get(Calendar.DAY_OF_YEAR) < 0) {
			adjust = -1;
		}
		
		artist.setAge(now.get(Calendar.YEAR) - birth.get(Calendar.YEAR) + adjust);
	}
	
}
