package com.pluralsight.service.listeners;

import java.util.logging.Level;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.pluralsight.model.Artist;
import com.pluralsight.utils.MyLogger;

public class ValidationListener {
	
	@PrePersist
	@PreUpdate
	private void validate(Artist artist) {
		MyLogger.log(Level.INFO, "Validating firstname and lastname of the Artist..");
		if(artist.getFirstName() == null || "".equals(artist.getFirstName())) {
			throw new IllegalArgumentException("Invalid firstname");
		}
		
		if(artist.getLastName() == null || "".equals(artist.getLastName())) {
			throw new IllegalArgumentException("Invalid lastname");
		}
	}
	
}
