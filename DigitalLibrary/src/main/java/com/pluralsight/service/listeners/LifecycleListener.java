package com.pluralsight.service.listeners;

import java.util.logging.Level;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

import com.pluralsight.utils.MyLogger;

public class LifecycleListener {
	
	@PrePersist
	void prePersist(Object object) {
		MyLogger.log(Level.INFO, "Pre Persist");
	}
	
	@PostPersist
	void postPersist(Object object) {
		MyLogger.log(Level.INFO, "Post Persist");
	}

}
