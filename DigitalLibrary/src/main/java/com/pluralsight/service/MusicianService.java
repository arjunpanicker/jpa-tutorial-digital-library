package com.pluralsight.service;

import javax.persistence.EntityManager;

import com.pluralsight.model.Musician;

public class MusicianService {
	
	EntityManager entityManager;
	
	public MusicianService(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public void createMusician(Musician musician) {
		entityManager.persist(musician);
	}

}
