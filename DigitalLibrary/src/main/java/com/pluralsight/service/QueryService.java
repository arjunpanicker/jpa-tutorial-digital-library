package com.pluralsight.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pluralsight.model.Book;
import com.pluralsight.model.CD;

public class QueryService {
	
	EntityManager entityManager;
	
	public QueryService(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public void queryBooks(Float unitCost, Integer nbOfPages) {
		System.out.println("Displaying Bookss with unitCost: " + unitCost.toString() + " and No. of pages: " + nbOfPages.toString() + "\n");
		TypedQuery<Book> query = entityManager.createQuery("SELECT b FROM Book b WHERE b.unitCost > :cost AND b.nbOfPages < :pages", Book.class);
		query.setParameter("cost", unitCost);
		query.setParameter("pages", nbOfPages);
		List<Book> bookList = query.getResultList();
		
		for (Book book : bookList) {
			System.out.println(book);
		}
	}
	
	public void queryBooksByPublicationDate(Date publicationDate) {
		System.out.println("Displaying Bookss with Publication Date: " + publicationDate.toString() + "\n");
		TypedQuery<Book> query = entityManager.createQuery("SELECT b FROM Book b WHERE b.publicationDate = :publication", Book.class);
		query.setParameter("publication", publicationDate, TemporalType.DATE);
		List<Book> bookList = query.getResultList();
		
		for (Book book : bookList) {
			System.out.println(book);
		}
	}
	
	public void queryCd(String genre) {
		
		System.out.println("Displaying CDs with genre: " + genre + "\n");
		String genreName = "%" + genre + "%";
		TypedQuery<CD> query = entityManager.createQuery("SELECT c FROM CD c WHERE lower(c.genre) like :genre", CD.class);
		query.setParameter("genre", genreName);
		List<CD> cdList = query.getResultList();
		
		for (CD cd : cdList) {
			System.out.println(cd);
		}
	}
	
	public void queryNamedCdAll() {
		System.out.println("Displaying All CDs..");
		Query query = entityManager.createNamedQuery("CD.All");
		
		@SuppressWarnings("rawtypes")
		List cdList = query.getResultList();
		
		for (int i=0; i < cdList.size(); i++) {
			CD cd = (CD) cdList.get(i);
			System.out.println(cd);
		}
	}
	
	public void queryNamedCdRock() {
		System.out.println("Displaying CDs with genre: Rock");
		TypedQuery<CD> query = entityManager.createNamedQuery("CD.FindRock", CD.class);
		
		List<CD> cdList = query.getResultList();
		
		for (CD cd : cdList) {
			System.out.println(cd);
		}
	}

}
