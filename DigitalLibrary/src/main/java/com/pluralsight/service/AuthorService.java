package com.pluralsight.service;

import javax.persistence.EntityManager;

import com.pluralsight.model.Author;

public class AuthorService {
	
	EntityManager entityManager;
	
	public AuthorService(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public Long createAuthor(Author author) {
		entityManager.persist(author);
		return author.getId();
	}
	
	public Author findAuthor(Long id) {
		Author author = entityManager.find(Author.class, id);
		
		if(author != null) {
			return author;
		}
		return null;
	}

}
