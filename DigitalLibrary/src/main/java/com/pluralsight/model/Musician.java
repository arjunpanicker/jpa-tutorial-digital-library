package com.pluralsight.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ExcludeDefaultListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.pluralsight.service.listeners.AgeCalculationListener;
import com.pluralsight.service.listeners.ValidationListener;

@Entity
@EntityListeners({ ValidationListener.class, AgeCalculationListener.class })
@ExcludeDefaultListeners
@NamedQuery(name = "Musician.findAll", query = "SELECT m FROM Musician m")
public class Musician extends Artist {

	@Id
	@SequenceGenerator(name = "musician_id", initialValue = 1, allocationSize = 1, sequenceName = "musician_id")
	@GeneratedValue(generator = "musician_id")
	private Long id;

	@Column(name = "preffered_instrument", length = 50)
	private String prefferedInstrument;
	
	@ManyToMany(mappedBy = "musicians", fetch = FetchType.EAGER)
	private Set<CD> cds = new HashSet<>();

	public Musician() {
	}

	public Musician(String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
	}

	public Musician(String firstName, String lastName, Date dateOfBirth, String prefferedInstrument) {
		setFirstName(firstName);
		setLastName(lastName);
		setDateOfBirth(dateOfBirth);
		setPrefferedInstrument(prefferedInstrument);
	}

	public Set<CD> getCds() {
		return cds;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	public String getPrefferedInstrument() {
		return prefferedInstrument;
	}

	public void setCds(Set<CD> cds) {
		this.cds = cds;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setPrefferedInstrument(String prefferedInstrument) {
		this.prefferedInstrument = prefferedInstrument;
	}

}
