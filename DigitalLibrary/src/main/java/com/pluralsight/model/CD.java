package com.pluralsight.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("C")
@Table(name = "cd")
@NamedQuery(name = "CD.All", query = "SELECT c FROM CD c")
@NamedQuery(name = "CD.FindRock", query = "SELECT c FROM CD c WHERE lower(c.genre) like '%rock%'")
public class CD extends Item {

	@Column(length = 255)
	private String genre;

	@Column(name = "total_duration", columnDefinition = "DECIMAL(5,2)")
	private Float totalDuration;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "CD_MUSICIAN", joinColumns = { @JoinColumn(name = "CD_ID") }, inverseJoinColumns = { @JoinColumn(name = "MUSICIAN_ID") })
	private Set<Musician> musicians = new HashSet<>();
	
	public CD() {}

	public CD(String genre) {
		setGenre(genre);
	}

	public CD(String genre, Float totalDuration) {
		setGenre(genre);
		setTotalDuration(totalDuration);
	}
	
	public CD(String title, String genre) {
		setTitle(title);
		setGenre(genre);
	}
	
	public CD(String title, String description, Float unitCost, Float totalDuration, String genre) {
		setTitle(title);
		setDescription(description);
		setUnitCost(unitCost);
		setTotalDuration(totalDuration);
		setGenre(genre);
	}
	
	public String getGenre() {
		return genre;
	}
	
	public Set<Musician> getMusicians() {
		return musicians;
	}

	public Float getTotalDuration() {
		return totalDuration;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setMusicians(Set<Musician> musicians) {
		this.musicians = musicians;
	}

	public void setTotalDuration(Float totalDuration) {
		this.totalDuration = totalDuration;
	}
	
	@Override
	public String toString() {
		return "Title: " + getTitle() + "\nDescription: " + getDescription() + "\n";
	}

}
