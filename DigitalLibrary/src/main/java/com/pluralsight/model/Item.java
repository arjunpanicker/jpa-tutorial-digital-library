package com.pluralsight.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

@MappedSuperclass
public class Item {
	
	@Id
	@SequenceGenerator(initialValue = 1, allocationSize = 1, name = "id", sequenceName = "id_generator")
	@GeneratedValue(generator = "id_generator")
	@Column(columnDefinition = "INTEGER")
	protected Long id;

	@Column(length = 100)
	protected String title;

	@Column(length = 3000)
	protected String description;

	@Column(name = "unit_cost", columnDefinition = "DECIMAL(6,2)")
	protected Float unitCost;

	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public Float getUnitCost() {
		return unitCost;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUnitCost(Float unitCost) {
		this.unitCost = unitCost;
	}
}
