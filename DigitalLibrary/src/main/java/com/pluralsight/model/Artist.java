package com.pluralsight.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class Artist {

	@Id
	@SequenceGenerator(name = "artist_id", initialValue = 1, allocationSize = 1, sequenceName = "artist_id")
	@GeneratedValue(generator = "artist_id")
	private Long id;
	
	@Column(name = "firstname", length = 50)
	private String firstName;

	@Column(name = "lastname", length = 50)
	private String lastName;

	@Column(name = "dob")
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;

	@Transient
	private Integer age;

	public Integer getAge() {
		return age;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public String getFirstName() {
		return firstName;
	}

	public Long getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
