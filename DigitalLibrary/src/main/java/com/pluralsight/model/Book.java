package com.pluralsight.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@DiscriminatorValue("B")
@Table(name = "book")
public class Book extends Item {

	@Column(length = 15)
	private String isbn;

	@Column(name = "nb_of_pages")
	private Integer nbOfPages;

	@Column(name = "publication_date")
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "BOOK_AUTHOR", joinColumns = {@JoinColumn(name = "BOOK_ID")}, inverseJoinColumns = {@JoinColumn(name = "AUTHOR_ID")})
	private Set<Author> authors;
	
	public Book( ) {}

	public Book(String isbn) {
		setIsbn(isbn);
	}

	public Book(String isbn, Integer nbOfPages) {
		setIsbn(isbn);
		setNbOfPages(nbOfPages);
	}
	
	public Book(String isbn, Integer nbOfPages, Date publicationDate) {
		setIsbn(isbn);
		setNbOfPages(nbOfPages);
		setPublicationDate(publicationDate);
	}
	
	public Book(String title, String isbn) {
		setTitle(title);
		setIsbn(isbn);
	}
	
	public Book(String title, String description, Float unitCost, String isbn, Integer nbOfPages, Date publicationDate) {
		setTitle(title);
		setDescription(description);
		setUnitCost(unitCost);
		setIsbn(isbn);
		setNbOfPages(nbOfPages);
		setPublicationDate(publicationDate);
	}

	public Set<Author> getAuthors() {
		return authors;
	}	
	
	
	public String getIsbn() {
		return isbn;
	}

	public Integer getNbOfPages() {
		return nbOfPages;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setNbOfPages(Integer nbOfPages) {
		this.nbOfPages = nbOfPages;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}
	
	@Override
	public String toString() {
		return "Title: " + getTitle() + "\nDescription: " + getDescription() + "\n";
	}
}
