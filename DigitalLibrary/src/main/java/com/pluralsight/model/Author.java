package com.pluralsight.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.pluralsight.service.listeners.AgeCalculationListener;
import com.pluralsight.service.listeners.ValidationListener;

@Entity
@EntityListeners({ ValidationListener.class, AgeCalculationListener.class })
public class Author extends Artist {

	@Id
	@SequenceGenerator(sequenceName = "author-id-gen", initialValue = 101, allocationSize = 1, name = "author_id")
	@GeneratedValue(generator = "author-id-gen")
	private Long id;

	@Column(length = 3000)
	private String bio;

	@ManyToMany(mappedBy = "authors", fetch = FetchType.EAGER)
	private Set<Book> books = new HashSet<>();

	public Author() {
	}

	public Author(String firstName, String lastName, String bio, Date dateOfBirth) {
		setFirstName(firstName);
		setLastName(lastName);
		setBio(bio);
		setDateOfBirth(dateOfBirth);
	}

	public String getBio() {
		return bio;
	}

	public Set<Book> getBooks() {
		return books;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Name: " + getFirstName() + " " + getLastName() + "\nDate of Birth: " + getDateOfBirth() + "\nAge: "
				+ getAge();
	}

}
