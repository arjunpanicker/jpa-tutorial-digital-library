package com.pluralsight.utils;

public final class Constants {
	
	private Constants() {
		throw new IllegalStateException("Utility Class");
	}
	
	public static final String DIGITAL_LIBRARY_UNIT = "digitalLibrary-persistenceUnit";
	public static final String DIGITAL_LIBRARY_TEST_UNIT = "digitalLibrary-persistenceUnit-Test";
	
	public static final String LOGGER_NAME = "main-logger";
	
	public static final String LOGGER_FILE_LOCATION = "D:/Eclipse Workspace/JPA Projects/jpa-tutorial-digital-library/DigitalLibrary/logs.log"; 
	
}
