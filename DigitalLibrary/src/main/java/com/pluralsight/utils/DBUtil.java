package com.pluralsight.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DBUtil {
	
	private DBUtil() {
		throw new IllegalStateException();
	}
	
	public static EntityManager getEntityManager(String persistenceUnitName) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName);
		return entityManagerFactory.createEntityManager();
	}
	
	public static EntityTransaction getEntityTransaction(EntityManager entityManager) {
		return entityManager.getTransaction();
	}
	
}
