package com.pluralsight.view;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.pluralsight.model.Author;
import com.pluralsight.model.Book;
import com.pluralsight.model.CD;
import com.pluralsight.model.Musician;
import com.pluralsight.service.ArtistService;
import com.pluralsight.service.ItemCreateService;
import com.pluralsight.utils.Constants;
import com.pluralsight.utils.DBUtil;
import com.pluralsight.utils.MyLogger;

public class Main {
	
	public static void main(String[] args) {
		
		EntityManager entityManager = DBUtil.getEntityManager(Constants.DIGITAL_LIBRARY_UNIT);
		EntityTransaction entityTransaction = DBUtil.getEntityTransaction(entityManager);

		ItemCreateService itemService = new ItemCreateService(entityManager);
		ArtistService artistService = new ArtistService(entityManager);

		entityTransaction.begin();
		itemService.createCD(addCD());
		itemService.createBooks(addBooks());
		artistService.createAuthors(addAuthors());
		entityTransaction.commit();

	}

	private static Set<Musician> addMusicians() {
		MyLogger.log(Level.INFO, "Creating Musicians List..");
		Set<Musician> musicians = new HashSet<>();
		musicians.add(new Musician("Arjun", "Panicker", date("1995-05-02"), "Piano"));
		musicians.add(new Musician("Juhi", "Srivastava", date("1994-12-28"), "Guitar"));
		musicians.add(new Musician("Akshay", "Kumar", date("1995-10-15"), "Harmonium"));
		musicians.add(new Musician("Shobit", "Chatterjee", date("1993-02-20"), "Drums"));
		musicians.add(new Musician("Abhishek", "Sharma", date("1995-08-05"), "Electric Guitar"));

		return musicians;
	}

	private static CD addCD() {
		MyLogger.log(Level.INFO, "Creating CDs List..");
		CD cd = new CD("Hello World", "Created by ShadowStorm banad", 99.24F, 30F, "pop");
		cd.setMusicians(addMusicians());
		return cd;
	}

	private static Set<Book> addBooks() {
		Set<Book> books = new HashSet<>();
		books.add(new Book("The Boy with a Broken Heart", "It's been two years since Raghu left his first love, Brahmi, on the edge of the roof one fateful night. He couldn't save her; he couldn't be with her. Having lost everything, Raghu now wants to stay hidden from the world. \r\n" + "However, the annoyingly persistent Advaita finds his elusiveness very attractive. And the more he ignores her, the more she's drawn to him till she bulldozes her way into an unlikely friendship. \r\n"
						+ "What attracts at first, begins to grate. Advaita can't help but want to know what Raghu has left behind, what he's hiding, and who broke his heart. She wants to love him back to life, but for that she needs to know what wrecked him in the first place. \r\n"
						+ "After all, the antidote to heartache is love.", 150.00F, "978-0143426585", 240, date("2017-11-27")));
		books.add(new Book("The Hobbit", "Bilbo Baggins is a hobbit who enjoys a comfortable, unambitious life, rarely travelling further than the pantry of his hobbit-hole in Bag End. But his contentment is disturbed when the wizard, Gandalf and a company of thirteen dwarves arrive on his doorstep one day to whisk him away on an unexpected journey �there and back again'. They have a plot to raid the treasure hoard of Smaug the Magnificent, a large and very dangerous dragon."
				+ " The prelude to The Lord of the Rings, The Hobbit has sold many millions of copies since its publication in 1937,"
				+ " establishing itself as one of the most beloved and influential books of the twentieth century.", 640.00F, "978-0008118044", 400, date("2014-11-25")));
		books.add(new Book("A Game of Thrones (A Song of Ice and Fire)", "Full of drama and adventure, rage and lust, mystery and romance, George R.R. Martin�s 'Game of Thrones: Song of Fire and Ice� (Book I) is regarded as one of the most intriguing and greatest epic of the modern era. Set in 12, 000BC, the seasons in this epic change after decades and bring with them mystery and death. The epic opens with the winter season fast approaching;"
				+ " though the human are protected and safe within the protective ice Wall of the kingdom, winter has arose the deadly forces that are continuously threatening the identity of the mortal power.", 610.00F, "978-0007428540", 864, date("2011-03-31")));
		books.add(new Book("Harry Potter and the Prisoner of Azkaban", "From the pen of one of the most renowned authors of the world, J. K. Rowling, comes the third instalment in the Harry Potter series, 'Harry Potter and the Prisoner of Azkaban�. After the life changing and near death experiences from the first two novels, the protagonist of the novel, Harry with his friends Ron and Hermione, enter their third year in the Hogwarts School of Witchcraft and Wizardry."
				+ " New adventures await them in their third year and everything changes when a mass-murderer going by the name of Sirius Black escapes the wizard prison of Azkaban and he is looking for Harry.", 834.00F, "978-1408855676", 320, date("2014-09-03")));
		
		return books;
	}

	private static Set<Author> addAuthors() {
		Set<Author> authors = new HashSet<>();
		authors.add(new Author("J.R.R.", "Tolkien", "J.R.R. Tolkien (1892-1973) was a distinguished academic, though he is best known for writing The Hobbit, The Lord of the Rings and The Silmarillion, plus other stories and essays. His books have been translated into over 50 languages and have sold many millions of copies worldwide."
				, date("1892-01-03")));
		authors.add(new Author("Durjoy", "Datta", "Durjoy Datta was born in New Delhi, and completed a degree in engineering and business management before embarking on a writing career. His first book-Of Course I Love You . . .-was published when he was twenty-one years old and was an instant bestseller. His successive novels-Now That You're Rich . . .;"
				+ " She Broke Up, I Didn't! . . .; Oh Yes, I'm Single! . . .; You Were My Crush . . .; If It's Not Forever . . .; Till the Last Breath . . .; Someone Like You; Hold My Hand; When Only Love Remains; World's Best Boyfriend; Our Impossible Love; The Girl of My Dreams; and The Boy Who Loved-have also found prominence on various bestseller lists, making him one of the highestselling authors in India."
				, date("1987-02-07")));
		authors.add(new Author("J.K.", "Rowling", "The Harry Potter series is the creation of British author J K Rowling. She began her Potter journey with the first book in the series Harry Potter and the Philosopher�s Stone, which was followed by six sequel titles in the series and three spin-off books,"
				+ " The Tales of Beedle and the Bard, Fantastic Beasts and where to find them and Quidditch Through the Ages. Her novels have been published worldwide and have been translated into over 78 languages. The success of Harry Potter have made J. K. Rowling one of the richest authors of the world"
				+ " and she can be considered a superstar in her own right. This particular edition has been published by the Bloomsbury Press and the latest edition was published in September 2014."
				, date("1965-07-31")));
		authors.add(new Author("George R. R.", "Martin", "George R.R. Martin: A professional writer-producer in Hollywood, George R.R. Martin rose to fame with his epic fantasy series A Song of Ice and Fire which has later been adapted by HBO into a famous television series,"
				+ " Game of Thrones. George R.R. Martin has also been in the list of Time�s 100 Most Influential People in the world."
				, date("1948-09-20")));
		
		return authors;
	}
	
	private static Date date(String date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {
			return dateFormat.parse(date);
		} catch (ParseException parseException) {
			MyLogger.log(Level.SEVERE, "Date parsing exception: {0}", parseException);
		}
		return null;
	}

}
